from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/holamundo', methods=['GET'])
def holamundo():
    datos = {
        'mensaje': 'Hola Mundo'
    }
    return jsonify(datos)


@app.route('/nombre', methods=['GET'])
def nombre():
    datos = {
        'mensaje': 'Josue Daniel Solis Osorio'
    }
    return jsonify(datos)


@app.route('/carnet', methods=['GET'])
def carnet():
    datos = {
        'mensaje': '202001574'
    }
    return jsonify(datos)









if __name__ == '__main__':
    app.run(debug=True)